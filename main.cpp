#include <string>
#include "make_double/make_double.h"
#include "print_string/print_string.h"

int main (int argc, char **)
{
	int res_double = makeDouble(8);
#ifdef USE_STRING_FUNC
	printString("result: " + std::to_string(res_double));
#endif
	return res_double;
}