set(SOURCES make_double.cpp)
set(HEADERS make_double.h)

add_library(make_double STATIC ${SOURCES} ${HEADERS})

target_include_directories(make_double PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})

